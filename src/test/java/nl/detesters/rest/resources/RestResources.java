package nl.detesters.rest.resources;

import java.net.URI;
import java.net.URISyntaxException;

/**
 *
 */
public class RestResources {
    public static URI jsonServer() throws URISyntaxException {
        return new URI("http://localhost:8081/");
    }

    public static URI customers() throws URISyntaxException {
        return new URI("http://localhost:8081/customers/");
    }
}

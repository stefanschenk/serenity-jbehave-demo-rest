package nl.detesters.rest.behaviours;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.http.ContentType;
import com.jayway.restassured.response.Response;
import net.serenitybdd.rest.SerenityRest;
import nl.detesters.rest.resources.RestResources;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Named;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.jbehave.core.model.ExamplesTable;
import org.jbehave.core.steps.Parameters;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.net.URISyntaxException;
import java.util.List;

import static com.jayway.restassured.config.EncoderConfig.encoderConfig;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.hamcrest.CoreMatchers.notNullValue;

/**
 *
 */
public class RestBehaviour {
    private final static Logger LOG = LoggerFactory.getLogger(RestBehaviour.class);

    Response getResponse;
    Response postResponse;
    int customerId;

    @Given("the rest service is available")
    public void givenTheRestServiceIsAvailable() throws URISyntaxException {
        SerenityRest.get(RestResources.jsonServer()).then().assertThat().statusCode(200);
    }

    @When("I add a new customer $customer")
    public void whenAddingNewCustomer(@Named("customer") final String customer) throws URISyntaxException {
        postResponse = SerenityRest
                .given()
                .config(RestAssured.config().encoderConfig(encoderConfig().appendDefaultContentCharsetToContentTypeIfUndefined(false)))
                .contentType(ContentType.JSON)
                .body(customer)
                .when()
                .post(RestResources.customers());

        postResponse.then().assertThat().statusCode(201);
    }

    @When("I get the list of customers")
    public void whenIGetTheListOfCustomers() throws URISyntaxException {
        getResponse = SerenityRest.get(RestResources.customers());
    }

    @Then("a customerlist is returned with at least the customer $customer")
    public void thenAtLeastOneCustomerIsReturned(@Named("customer") final ExamplesTable customer) {
        List<Parameters> parametersList = customer.getRowsAsParameters();

        for (Parameters parameters : parametersList) {
            getResponse.then()
                    .body("initials", hasItem(parameters.valueAs("initials", String.class)))
                    .body("name", hasItem(parameters.valueAs("name", String.class)));
        }
    }

    @Then("I got a response with an id")
    public void thenGotResponseWithId() {
        customerId = postResponse
                .then()
                .body("id", notNullValue())
                .extract().path("id");
    }

    @Then("customer $name can be retrieved with this id")
    public void thenTheCustomerCanBeRetrievedWithGivenId(@Named("name") final String name) throws URISyntaxException {
        SerenityRest
                .given()
                .baseUri(RestResources.customers().toString())
                .get(String.valueOf(customerId))
                .then()
                .statusCode(200)
                .body("name", equalTo(name));
    }
}

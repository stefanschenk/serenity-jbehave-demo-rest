Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: Get Customers
Given the rest service is available
When I get the list of customers
Then a customerlist is returned with at least the customer
|initials|name  |
|D.E.    |Tester|
Meta:

Narrative:
As a user
I want to perform an action
So that I can achieve a business goal

Scenario: Add a new customer
Given the rest service is available
When I add a new customer
{
  "initials": "O.",
  "name": "Stilton",
  "address": {
    "street": "George Street",
    "number": "11",
    "zipcode": "PE2 9PD",
    "city": "Peterborough"
  }
}
Then I got a response with an id
And customer Stilton can be retrieved with this id